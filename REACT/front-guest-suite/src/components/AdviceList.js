import React from 'react';
import {axiosService} from "../helpers/axios";


export default class AdviceList extends React.Component {
    state = {
        advices: []
    };

    componentDidMount() {
        axiosService.CONNEXION_API.get('advices')
            .then(res => {
                const advices = axiosService.handleHydraResponse(res);
                console.log(advices);
                this.setState({ advices });
            })
    }

    render() {
        return (
            <ul>
                { this.state.advices.map(advice => <li>{advice.comment}</li>)}
            </ul>
        )
    }
}