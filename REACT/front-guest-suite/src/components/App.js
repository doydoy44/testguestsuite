import React from 'react';
import './App.css';
import AdviceList from "./AdviceList";

function App() {
  return (
    <div className="App">
        <div className="container">
            <div className="jumbotron bg-info">
                <h1 className="display-3">Test API pour Guest Suite</h1>
                <AdviceList/>
            </div>
        </div>
    </div>
  );
}

export default App;
