import axios from 'axios';
import {API_BASE_URL} from '../config/config';

const API_URL = API_BASE_URL + '/api/';

const CONNEXION_API = axios.create({
    baseURL: API_URL,
    headers: {
        'Content-Type': 'application/json',
    },
});


export const axiosService = {
    CONNEXION_API,
    handleHydraResponse,
};

/**
 * Récupération du json généré par api-platform
 * @param response
 * @returns {*}
 */
function handleHydraResponse(response) {

    let data = response.data;
    if (response.statusText !== "OK") {

        const error = (data && data.message) || response.statusText;
        return Promise.reject(error);
    }

    return data['hydra:member'];
}
