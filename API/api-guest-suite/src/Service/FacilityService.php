<?php

namespace App\Service;

use App\Repository\AdviceRepository;
use App\Repository\FacilityRepository;
use Doctrine\ORM\EntityManagerInterface;

class FacilityService
{

    /**
     * @var AdviceRepository
     */
    private $adviceRepository;

    public function __construct(
        AdviceRepository $adviceRepository
    )
    {
        $this->adviceRepository = $adviceRepository;
    }
    public function getGolbalRating(\App\Entity\Facility $facility ) {
        $advices = $this->adviceRepository->findBy(['facility' => $facility]);
        $platforms = [];
        foreach($advices as $advice){
            $platform = $advice->getPlatform();
            if (!isset($platforms[$platform->getName()])) {
                $platforms[$platform->getName()] = [
                    'ratingTotal' => 0,
                    'ratingCount' => 0,
                    'globalRating' => 0,
                ];
            }
            $platforms[$platform->getName()]['ratingTotal'] += $advice->getRating();
            $platforms[$platform->getName()]['ratingCount']++;
            $platforms[$platform->getName()]['globalRating'] = $platforms[$platform->getName()]['ratingTotal'] / $platforms[$platform->getName()]['ratingCount'];
        }

        return $platforms;

//        return $global_rating;
    }

}