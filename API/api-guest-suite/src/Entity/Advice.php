<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     collectionOperations={"get"},
 *     normalizationContext={"groups"={"advice"}},
 *     )
 * @ORM\Entity(repositoryClass="App\Repository\AdviceRepository")
 */
class Advice
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     * @Groups({"advice"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Groups({"advice"})
     */
    private $comment;

    /**
     * @ORM\Column(type="integer")
     *
     * @Groups({"advice"})
     */
    private $rating;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Author", inversedBy="advices")
     * @ORM\JoinColumn(nullable=false)
     *
     * @ApiSubresource()
     * @Groups({"advice"})
     */
    private $author;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Facility", inversedBy="advices")
     * @ORM\JoinColumn(nullable=false)
     *
     * @ApiSubresource()
     * @Groups({"advice"})
     */
    private $facility;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Platform", inversedBy="advices")
     * @ORM\JoinColumn(nullable=false)
     *
     * @ApiSubresource()
     * @Groups({"advice"})
     */
    private $platform;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateCreate;

    public function __construct()
    {
        $now = new \DateTime('now');
        $this->setDateCreate($now);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getRating(): ?int
    {
        return $this->rating;
    }

    public function setRating(int $rating): self
    {
        $this->rating = $rating;

        return $this;
    }

    public function getAuthor(): ?Author
    {
        return $this->author;
    }

    public function setAuthor(?Author $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getFacility(): ?Facility
    {
        return $this->facility;
    }

    public function setFacility(?Facility $facility): self
    {
        $this->facility = $facility;

        return $this;
    }

    public function getPlatform(): ?Platform
    {
        return $this->platform;
    }

    public function setPlatform(?Platform $platform): self
    {
        $this->platform = $platform;

        return $this;
    }

    public function getDateCreate(): ?\DateTimeInterface
    {
        return $this->dateCreate;
    }

    public function setDateCreate(\DateTimeInterface $dateCreate): self
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }
}
