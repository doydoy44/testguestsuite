<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     itemOperations={
 *        "getGolbalRatin"={
 *          "route_name"="globals_rating",
 *          "swagger_context" = {
 *            "parameters" = {
 *              {
 *                "name" = "id",
 *                "in" = "path",
 *                "required" = "true",
 *                "type" = "string"
 *              }
 *            },
 *            "responses" = {
 *              "200" = {
 *                "description" = "The globals Rating has been returned in the response"
 *              }
 *            }
 *          }
 *        }
 *     })
 * @ORM\Entity(repositoryClass="App\Repository\FacilityRepository")
 */
class Facility
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     * @Groups({"advice"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Groups({"advice"})
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Advice", mappedBy="facility")
     */
    private $advices;

    public function __construct()
    {
        $this->advices = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Advice[]
     */
    public function getAdvices(): Collection
    {
        return $this->advices;
    }

    public function addAdvice(Advice $advice): self
    {
        if (!$this->advices->contains($advice)) {
            $this->advices[] = $advice;
            $advice->setFacility($this);
        }

        return $this;
    }

    public function removeAdvice(Advice $advice): self
    {
        if ($this->advices->contains($advice)) {
            $this->advices->removeElement($advice);
            // set the owning side to null (unless already changed)
            if ($advice->getFacility() === $this) {
                $advice->setFacility(null);
            }
        }

        return $this;
    }
}
