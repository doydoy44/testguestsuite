<?php

namespace App\DataFixtures;

use App\Entity\Advice;
use App\Entity\Author;
use App\Entity\Facility;
use App\Entity\Platform;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $authors    = $this->getAuthorData();
        $platforms  = $this->getPlatformData();
        $facilities = $this->getFacilityData();

        foreach ($authors as [$name, $email]) {
            $author = new Author();
            $author->setName($name);
            $author->setEmail($email);
            $manager->persist($author);
            $this->addReference($name, $author);
        }
        foreach ($facilities as [$name]) {
            $facility = new Facility();
            $facility->setName($name);
            $manager->persist($facility);
            $this->addReference($name, $facility);
        }
        foreach ($platforms as [$name]) {
            $platform = new Platform();
            $platform->setName($name);
            $manager->persist($platform);
            $this->addReference($name, $platform);
        }
        foreach ($this->getAdviceData() as [$dateCreateISO, $comment, $rating]) {
            $advice = new Advice();
            $advice->setComment($comment);
            $advice->setRating($rating);
            $dateCreate = new \DateTime($dateCreateISO);
            $advice->setDateCreate($dateCreate);
            // Récupération d'un autheur d'avis existant
            $author   = $this->getReference($authors[rand(0, count($authors) - 1)][0]);
            // Récupération d'une plateforme existante
            $platform = $this->getReference($platforms[rand(0, count($platforms) - 1)][0]);
            // Récupération d'un établissement existant
            $facility = $this->getReference($facilities[rand(0, count($facilities) - 1)][0]);

            $advice->setAuthor($author);
            $advice->setFacility($facility);
            $advice->setPlatform($platform);

            $manager->persist($advice);
        }
        $manager->flush();
    }

    private function getAuthorData(): array
    {
        return [
            // $authorData = [$name, $email];
            ['Laurent Papillon', 'laurentpapillon@free.fr'],
            ['Jane Doe', 'jane_admin@symfony.com'],
            ['Tom Doe', 'tom_admin@symfony.com'],
            ['John Doe', 'john_user@symfony.com'],
        ];
    }

    private function getFacilityData(): array
    {
        return [
            // $facilityData = [$name];
            ['Ibis' ],
            ['B&B' ],
            ['Mercure' ],
            ['Hôtel de la plage' ],
        ];
    }
    private function getPlatformData(): array
    {
        // $platformData = [$name];
        return [
            ['Google' ],
            ['Pages Jaunes' ],
            ['TripAdvisor' ]
        ];
    }

    private function getAdviceData(): array
    {
        // $platformData = [$dateCreateISO, $comment, $rating];
        return [
            ['2019-06-01 12:01:10', 'Super Accueil, nous reviendons', 8],
            ['2019-06-02 11:30:36', 'Après 3 jours, on fait partie de la famille :D', 10],
            ['2019-06-05 15:24:21', 'RAS', 8],
            ['2019-06-10 13:32:52', 'Propre mais ne correspond pas à la description', 5],
            ['2019-06-12 18:41:08', 'A fuir !!!', 0],
            ['2019-06-14 19:02:17', 'C\'est l\'époque des stagiaire et ça se ressent', 2],
        ];
    }
}
