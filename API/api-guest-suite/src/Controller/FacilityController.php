<?php

namespace App\Controller;

use App\Entity\Facility;
use App\Service\FacilityService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class FacilityController extends AbstractController
{
    /**
     * @Route(
     *     name="globals_rating",
     *     path="api/facility/{id}/getglobalsrating",
     *     methods={"GET"},
     *     defaults={
     *       "_controller"="\App\Controller\FacilityController::getGolbalRating",
     *       "_api_resource_class"="App\Entity\Facility",
     *       "_api_item_operation_name"="getGolbalRating"
     *     }
     *   )
     */
    public function getGolbalRating(Facility $data, FacilityService $facilityService) {
        $globalsRating = $facilityService->getGolbalRating($data);
        return $this->json([
            'id' => $data->getId(),
            'globals_rating' => $globalsRating,
        ]);
    }
}

