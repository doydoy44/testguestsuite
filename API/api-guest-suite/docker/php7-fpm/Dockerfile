FROM php:7.2-fpm

MAINTAINER doydoy44 <doydoy44@free.fr>

ENV DEBIAN_FRONTEND noninteractive

############
# BACKEND #
############

ENV APCU_VERSION 5.1.8
ENV APCU_BC_VERSION 1.0.3

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    sudo wget curl git ca-certificates

# Install getText
RUN docker-php-ext-install gettext pcntl

# Install zip
RUN apt-get install -y zlib1g-dev \
    && docker-php-ext-install zip

# Install apc & opcache
RUN pecl install apcu-${APCU_VERSION} \
    && pecl install apcu_bc-${APCU_BC_VERSION} \
    && docker-php-ext-enable --ini-name 05-docker-opcache.ini opcache \
    && docker-php-ext-enable --ini-name 10-docker-apcu.ini apcu \
    && docker-php-ext-enable --ini-name 20-docker-apc.ini apc

# Install intl
RUN apt-get update \
  && apt-get install -y zlib1g-dev libicu-dev g++ \
  && docker-php-ext-configure intl \
  && docker-php-ext-install intl

# Install PDO
RUN apt-get install -y libpq-dev \
    && docker-php-ext-install pdo_pgsql pdo pdo_mysql

# Install composer
RUN curl -sS https://getcomposer.org/installer | php \
    && mv composer.phar /usr/local/bin/composer

# Install phpunit
RUN curl -SL "https://phar.phpunit.de/phpunit-old.phar" -o /usr/local/bin/phpunit \
	&& chmod +x /usr/local/bin/phpunit

RUN composer global require "hirak/prestissimo:^0.3" --prefer-dist --no-progress --no-suggest --optimize-autoloader --classmap-authoritative \
	&& composer clear-cache

RUN docker-php-ext-install exif

RUN apt-get update \
    && apt-get install -y \
        libpng-dev \
        libjpeg-dev \
    && docker-php-ext-install gd \
    &&  docker-php-ext-enable gd

#RUN apt-get update \
#    && apt-get install -y \
## for mongodb
#        libssl-dev \
#    && pecl install mongodb \
## prooph mongo db adapter uses deprecated ext currently
#    && rm -r /var/lib/apt/lists/* \
#    && docker-php-ext-enable mongodb
# for mongodb
RUN apt-get install -y libssl-dev && pecl install mongodb && docker-php-ext-enable mongodb

# for soap
RUN apt-get update \
    && apt-get install -y \
        libxml2 \
        libxml2-dev \
    && docker-php-ext-configure soap --enable-soap \
    && docker-php-ext-install \
        soap

RUN apt-get update \
    && apt-get install -y \
        libmagickwand-dev --no-install-recommends \
    && pecl install imagick \
    && docker-php-ext-enable imagick

# Install xdebug
RUN apt-get update \
    && apt-get install -y \
    && pecl install xdebug \
    && docker-php-ext-enable xdebug

# Install php.ini config
COPY php.ini /usr/local/etc/php/php.ini

WORKDIR /var/www/symfony



